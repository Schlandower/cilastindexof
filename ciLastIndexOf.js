if(!String.prototype.ciLastIndexOf) {
	String.prototype.ciLastIndexOf = function (fNeedle, fOffset) {
		var hs = this.toLowerCase();
		if(fNeedle !== null && fNeedle !== undefined && fNeedle !== '') {
			var ne = fNeedle.toLowerCase();
			if(fOffset === undefined || fOffset === null || fOffset === '') {
				fOffset = 0;
			}
			return hs.lastIndexOf(ne, fOffset);
		} else {
			return -1;
		}
	};
}