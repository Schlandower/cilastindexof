Case insensitive lastIndexOf module for node.js
Usage:
require('ciLastIndexOf');
if(inputString.ciLastIndexOf('searchString') !== -1) {}
Returns the index of the first character of the last occurrence of the search string if it exists, or -1.